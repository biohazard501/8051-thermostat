//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------

#include <c8051f020.h>                 // SFR declarations
//#include <compiler_defs.h>
//#include <stdio.h>
//#include "lcd.h"					   // Adding this library for LCD control

//-----------------------------------------------------------------------------
// 16-bit SFR Definitions for 'F02x
//-----------------------------------------------------------------------------

//sfr16 ADC0     = 0xbe;                 // ADC0 data
sfr16 RCAP2    = 0xca;                 // Timer2 capture/reload
//sfr16 RCAP3    = 0x92;                 // Timer3 capture/reload
sfr16 TMR2     = 0xcc;                 // Timer2
sfr16 TMR3     = 0x94;                 // Timer3

//LCD Module Connections
/*
sbit RS = P1^0;                                                                   
sbit EN = P1^2;                            
sbit D0 = P2^0;
sbit D1 = P2^1;
sbit D2 = P2^2;
sbit D3 = P2^3;
sbit D4 = P2^4;
sbit D5 = P2^5;
sbit D6 = P2^6;
sbit D7 = P2^7;
*/
//-----------------------------------------------------------------------------
// Global Constants
//-----------------------------------------------------------------------------

#define BAUDRATE     9600	           // Baud rate of UART in bps
#define SYSTEMCLOCK       (22118400L)  // External crystal oscillator frequency

//-----------------------------------------------------------------------------
// Function Prototypes
//-----------------------------------------------------------------------------

void OSCILLATOR_Init (void);           
void PORT_Init (void);
void UART1_Init (void);
void ADC1_Init (void);
void TIMER3_Init (int counts);
void Timer3_ISR ();
void Wait_MS (unsigned int ms);
void TransmitData (void);

//-----------------------------------------------------------------------------
// Global Variables
//-----------------------------------------------------------------------------

#define UART_RX_BUFFERSIZE 40
unsigned char UART_Rx_Buffer[UART_RX_BUFFERSIZE];
unsigned char UART_Rx_Buffer_Size = 0;
unsigned char UART_Rx_Input_First = 0;
unsigned char UART_Rx_Output_First = 0;

#define UART_TX_BUFFERSIZE 20
unsigned char UART_Tx_Buffer[UART_TX_BUFFERSIZE];
unsigned char UART_Tx_Buffer_Size = 0;
unsigned char UART_Tx_Input_First = 0;
unsigned char UART_Tx_Output_First = 0;

unsigned char TX_Ready = 1;
static char Byte;
unsigned char Dial_Reading;
unsigned char Temp_Reading;

//-----------------------------------------------------------------------------
// main() Routine
//-----------------------------------------------------------------------------

void main (void)
{
	int i = 0;
	int j = 0;
	int k = 0;

//	char startByteIndex = -1;
//	char rxPacketStartIndex = 0;

//	bit shiftArray = 0;

//	bit shouldBuzzOnEmpty = 1;

	unsigned short averageTemp = 0;
	unsigned short controlUnitState = 0x00;

	WDTCN = 0xDE;                       // Disable watchdog timer
	WDTCN = 0xAD;

	OSCILLATOR_Init ();                 // Initialize oscillator
	PORT_Init ();                       // Initialize crossbar and GPIO
	UART1_Init ();                      // Initialize UART1 for ZigBee

	// Timer 3 is used for ADC1
	//TIMER3_Init (SYSTEMCLOCK/12/10);   // Initialize Timer3 to overflow at
	                              // sample rate

	//ADC1_Init ();                       // Init ADC

	EA = 1;                             // Enable global interrupts

	P5 = P5 & 0xFF;

	// Flash the LEDs on bootup for visual conf that the thing is running
/*	for ( i = 0; i < 10; i++) 
	{
		Wait_MS(50);
		P5 |= 0xF0;
		Wait_MS(50);
	  	P5 = 0x00;
	}
*/
	i = 0;

	while (1)
	{
		//EA = 0;

		// Write the initial text into the LCD display. maybe need to put in second c file.
		/*Lcd4_Set_Cursor(1,1);
		Lcd4_Write_String("Temp: ");
		Lcd4_Set_Cursor(1,8);
		Lcd4_Write_String((char*)averageTemp);

		if (controlUnitState & 0x01)
		{
			Lcd4_Set_Cursor(1,11);
			Lcd4_Write_String("ON");			
		}
		else
		{
			Lcd4_Set_Cursor(1,11);
			Lcd4_Write_String("OFF");			
		}

		Lcd4_Set_Cursor(2,11);
		Lcd4_Write_String("Set: ");
		Lcd4_Write_String((char*)Dial_Reading);
		

		if (controlUnitState & 0x02)
		{
			Lcd4_Set_Cursor(1,8);
			Lcd4_Write_String("NC");			
		}
		else
		{
			Lcd4_Set_Cursor(1,8);
			Lcd4_Write_String("  ");			
		}
*/
		// Check for a ZigBee Rx Packet API frame that contains a combo set/actual temp pair, 
		// and if it is, read the first two bytes
		if (UART_Rx_Buffer_Size == 18 && UART_Rx_Buffer[0] == 0x7E && UART_Rx_Buffer[3] == 0x90)
		{
			// Assign the control unit's computed temp average to a variable
			averageTemp = UART_Rx_Buffer[15];
			controlUnitState = UART_Rx_Buffer[16];
			UART_Rx_Buffer_Size = 0; // reset buffer
		}
		else 
		{
			UART_Rx_Buffer_Size = 0;
		}
//  		EA = 1;

		// Check the control unit state for whether the A/C unit is
		// on and cooling the room or off
		/*
		if (controlUnitState & 0x01) 
		{
			P5 |= 0x10;
		}
		else 
		{
			P5 &= ~0x10;
		}
*/
//		EA = 0;

		// Check the control unit state for coolant remaining or empty
		/*
		if (controlUnitState & 0x02)
		{
			P5 &= ~0x20;
			
			// this condition will get hit over and over so let's not
			// keep sounding the buzzer each time. Do it once.
			if (shouldBuzzOnEmpty) 
			{			
				shouldBuzzOnEmpty = 0;
			}
		}
		else 
		{
			P5 |= 0x20;

			// coolant remains so make sure next time it goes 'dry' we
			// activate the buzzer
			shouldBuzzOnEmpty = 1;
		}
*/
//		EA = 1;

		if(TX_Ready == 1 && j == 0)
		{	
			TransmitData();
			UART_Tx_Buffer_Size = 0;
		}

		j++;

		if (j == 12) j = 0;

		Wait_MS(150);           // Wait some time before taking
                                       // another sample
	}
}

//-----------------------------------------------------------------------------
// Initialization Subroutines
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// OSCILLATOR_Init
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// This routine initializes the system clock to use an 22.1184MHz crystal
// as its clock source.
//
//
//-----------------------------------------------------------------------------
void OSCILLATOR_Init (void)
{
   int i;                              // Software timer

   OSCICN |= 0x80;                     // Enable the missing clock detector

   // Initialize external crystal oscillator to use 22.1184 MHz crystal

   OSCXCN = 0x67;                      // Enable external crystal osc.
   for (i=0; i < 256; i++);            // Wait at least 1ms
   while (!(OSCXCN & 0x80));           // Wait for crystal osc to settle
   OSCICN |= 0x08;                     // Select external clock source
   OSCICN &= ~0x04;                    // Disable the internal osc.
}

//-----------------------------------------------------------------------------
// PORT_Init
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// This function configures the crossbar and GPIO ports.
//
// P0.2   digital   push-pull     UART TX
// P0.3   digital   open-drain    UART RX

//-----------------------------------------------------------------------------
void PORT_Init (void)
{
   XBR0    = 0x04;                     // Route UART0 to crossbar
	XBR1 = 0x00;
   XBR2    = 0x44;                    // Enable crossbar,  weak pull-ups,
   									   // and enable UART1

   P0MDOUT |= 0x04;     		// Set UART TX pins to push-pull on port 0

//   P0MDOUT |= 0x01;                    // Set TX1 pin to push-pull
//   P0MDOUT |= 0x40;                    // Set P0.6 to digital out
//   P1MDOUT |= 0x40;                    // Set P1.6(LED) to push-pull

	//P1MDOUT |= 0x07;		// Set port 1 pin 1, 2, 3 to output push-pull digital

   	P2MDOUT = 0x00;
	P3MDOUT = 0x00;

	P74OUT = 0x08;

	P5 |= 0x0F;
	P4 = 0xFF;

	//P2MDOUT = 0xFF;

	// Analog inputs - take care to change otherwise will destroy TM36
	//P1MDIN &= ~0x40;
	//P1MDIN &= ~0x80;
	//P1 |= 0x40;
	//P1 |= 0x80;
}

//-----------------------------------------------------------------------------
// UART1_Init
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
// Configure the UART1 using Timer1, for <baudrate> and 8-N-1.
// This routine configures the UART1 based on the following equation:
//
// Baud = (2^SMOD1/32)*(SYSTEMCLOCK*12^(T1M-1))/(256-TH1)
//
// This equation can be found in the datasheet, Mode1 baud rate using timer1.
// The function select the proper values of the SMOD1 and T1M bits to allow
// for the proper baud rate to be reached.
//-----------------------------------------------------------------------------
void UART1_Init (void)
{
   SCON1   = 0x50;                     // SCON1: mode 1, 8-bit UART, enable RX

   TMOD   &= ~0xF0;
   TMOD   |=  0x20;                    // TMOD: timer 1, mode 2, 8-bit reload


   PCON |= 0x10;                    // SMOD1 (PCON.4) = 1 --> UART1 baudrate
                                       // divide-by-two disabled
   CKCON |= 0x10;                   // Timer1 uses the SYSTEMCLOCK
   TH1 = - ((SYSTEMCLOCK/BAUDRATE)/16);

   TL1 = TH1;                          // init Timer1
   TR1 = 1;                            // START Timer1
   TX_Ready = 1;                       // Flag showing that UART can transmit
   EIE2     = 0x40;                    // Enable UART1 interrupts

   EIP2    = 0x40;                     // Make UART high priority

}

//-----------------------------------------------------------------------------
// TIMER3_Init
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   :
//   1)  int counts - calculated Timer overflow rate
//                    range is postive range of integer: 0 to 32767
//
// Configure Timer3 to auto-reload at interval specified by <counts> (no
// interrupt generated) using SYSCLK as its time base.
//
//-----------------------------------------------------------------------------
/* void TIMER3_Init (unsigned int counts)
{

   TMR3CN = 0x00;                      // Stop Timer3; Clear TF3; set sysclk
                                       // as timebase
	TMR3RLL = -counts;
	TMR3 = 0xFFFF;
	EIE2 |= 0x01;
	TMR3CN |= 0x04;
}
*/
//-----------------------------------------------------------------------------
// Interrupt Service Routines
//-----------------------------------------------------------------------------
/*
void Timer3_ISR(void) interrupt 14
{
	float temp = 0.0f;
	float dial = 0.0f;

	TMR3CN &= ~(0x80);

	//while((ADC1CN & 0x20) == 0);	

	if (AMX1SL == 0x06)
	{
//		Temp_Reading = ADC1;
		temp = ((float)ADC1 - 91.0f) * 1.8f;
		Temp_Reading = (short)temp + 32;

		AMX1SL = 0x07;
	}
	else
	{
		dial = (((float)ADC1 - 14.0f) * 0.16597510) + 50;

		if (dial < 50.0f) dial = 50.0f;
		else if (dial > 89.5f) dial = 90.0f;

		Dial_Reading = (short)dial;
		AMX1SL = 0x06;
	}

	ADC1CN &= 0xDF;
}
*/
//-----------------------------------------------------------------------------
// ADC1_Init
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters   : None
//
//-----------------------------------------------------------------------------
/*
void ADC1_Init (void)
{
	REF0CN = 0x03;

	ADC1CF = 0x81;//(SYSCLK/SAR_CLK) << 3;     // ADC conversion clock = 2.5MHz
   	//ADC1CF |= 0x00;

	AMX1SL = 0x06;
	ADC1CN = 0x82; // enable ADC interrupts
}
*/

//-----------------------------------------------------------------------------
// Interrupt Service Routines
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// UART1_Interrupt
//-----------------------------------------------------------------------------
//
// This routine is invoked whenever a byte is received from UART or transmitted.
//
//-----------------------------------------------------------------------------

void UART1_Interrupt (void) interrupt 20
{
   if ((SCON1 & 0x01) == 0x01)
   {
      // Check if a new word is being entered
      if( UART_Rx_Buffer_Size == 0)  
	  {
         UART_Rx_Input_First = 0; 
	  } 

      SCON1 = (SCON1 & 0xFE);          //RI1 = 0;
      Byte = SBUF1;                    // Read a character from Hyperterminal
		
      if (UART_Rx_Buffer_Size < UART_RX_BUFFERSIZE)
      {
         UART_Rx_Buffer[UART_Rx_Input_First] = Byte;  // Store character

         UART_Rx_Buffer_Size++;            // Update array's size

         UART_Rx_Input_First++;            // Update counter
      }
   }

   if ((SCON1 & 0x02) == 0x02)         // Check if transmit flag is set
   {
      SCON1 = (SCON1 & 0xFD);
      if (UART_Tx_Buffer_Size > 0)        // If buffer not empty
      {
         SBUF1 = UART_Tx_Buffer[UART_Tx_Output_First];

         UART_Tx_Output_First++;           // Update counter
         UART_Tx_Buffer_Size--;            // Decrease array size
      }
      else
      {
         UART_Tx_Output_First = 0;
         UART_Tx_Buffer_Size = 0;           // Set the array size to 0
         TX_Ready = 1;                   // Indicate transmission complete
      }
   }
}

//-----------------------------------------------------------------------------
// Support Subroutines
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// TransmitData
//-----------------------------------------------------------------------------
//
// Transmits a ZigBee Transmit Request frame with the "set" temp in payload 
// byte 0 the actual room temp from the theromstat's temp sensor in byte 1
//
//-----------------------------------------------------------------------------

void TransmitData()
{
	short i = 0;
	int sum = 0;

	UART_Tx_Buffer[0] = 0x7E; // start byte
	UART_Tx_Buffer[1] = 0x00; // Length MSB
	UART_Tx_Buffer[2] = 0x10; // Length LSB
	UART_Tx_Buffer[3] = 0x10; // frame type (0x10 = transmit request)
	UART_Tx_Buffer[4] = 0x00; // frame ID - 0 means no ack

	UART_Tx_Buffer[5] = 0xFF;	// start 64-bit addr
	UART_Tx_Buffer[6] = 0xFF;
	UART_Tx_Buffer[7] = 0xFF;
	UART_Tx_Buffer[8] = 0xFF;
	UART_Tx_Buffer[9] = 0xFF;
	UART_Tx_Buffer[10] = 0xFF;
	UART_Tx_Buffer[11] = 0xFF;
	UART_Tx_Buffer[12] = 0xFF;

	UART_Tx_Buffer[13] = 0xFF;	// 16-bit net addr
	UART_Tx_Buffer[14] = 0xFE;

	UART_Tx_Buffer[15] = 0x00;
	UART_Tx_Buffer[16] = 0x01; // do not retry

	UART_Tx_Buffer[17] = 0x37;//Dial_Reading;
	UART_Tx_Buffer[18] = 0x38;//Temp_Reading;

	// compute the checksum per the ZigBee API spec
	// Algorithm: Add all bytes except the first three, then remove
	// all but the first 8 bits and subtract that value from 0xFF
	for ( i = 3; i <= 18; i++ )
	{
		sum = sum + (int)UART_Tx_Buffer[i];
	}

	UART_Tx_Buffer[19] = 0xFF - (sum & ~0xFF00); // checksum
	UART_Tx_Buffer_Size = 19;

	TX_Ready = 0;
	SCON1 = (SCON1 | 0x02);
}


//-----------------------------------------------------------------------------
// Wait_MS
//-----------------------------------------------------------------------------
//
// Return Value : None
// Parameters:
//   1) unsigned int ms - number of milliseconds of delay
//                        range is full range of integer: 0 to 65335
//
// This routine inserts a delay of <ms> milliseconds.
//
//-----------------------------------------------------------------------------
void Wait_MS(unsigned int ms)
{

   CKCON &= ~0x20;                     // use SYSCLK/12 as timebase

   RCAP2 = -(SYSTEMCLOCK/1000/12);          // Timer 2 overflows at 1 kHz
   TMR2 = RCAP2;

   ET2 = 0;                            // Disable Timer 2 interrupts

   TR2 = 1;                            // Start Timer 2

   while(ms)
   {
      TF2 = 0;                         // Clear flag to initialize
      while(!TF2);                     // Wait until timer overflows
      ms--;                            // Decrement ms
   }

   TR2 = 0;                            // Stop Timer 2

}

//-----------------------------------------------------------------------------
// End Of File
//-----------------------------------------------------------------------------